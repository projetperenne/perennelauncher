﻿namespace WindowsFormsApplication3
{
    partial class Pérenne
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Pérenne));
            this.metroProgressBar1 = new MetroFramework.Controls.MetroProgressBar();
            this.CheckFile = new MetroFramework.Controls.MetroButton();
            this.Start = new MetroFramework.Controls.MetroButton();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.metroProgressBar2 = new MetroFramework.Controls.MetroProgressBar();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.CancelButton = new MetroFramework.Controls.MetroButton();
            this.SuspendLayout();
            // 
            // metroProgressBar1
            // 
            this.metroProgressBar1.Location = new System.Drawing.Point(105, 260);
            this.metroProgressBar1.Name = "metroProgressBar1";
            this.metroProgressBar1.Size = new System.Drawing.Size(435, 35);
            this.metroProgressBar1.Step = 1;
            this.metroProgressBar1.Style = MetroFramework.MetroColorStyle.Lime;
            this.metroProgressBar1.TabIndex = 0;
            this.metroProgressBar1.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroProgressBar1.UseCustomBackColor = true;
            // 
            // CheckFile
            // 
            this.CheckFile.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(20)))));
            this.CheckFile.Location = new System.Drawing.Point(581, 260);
            this.CheckFile.Name = "CheckFile";
            this.CheckFile.Size = new System.Drawing.Size(94, 35);
            this.CheckFile.Style = MetroFramework.MetroColorStyle.White;
            this.CheckFile.TabIndex = 6;
            this.CheckFile.TabStop = false;
            this.CheckFile.Text = "Check";
            this.CheckFile.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.CheckFile.UseCustomBackColor = true;
            this.CheckFile.UseSelectable = true;
            this.CheckFile.Click += new System.EventHandler(this.CheckFile_Click);
            // 
            // Start
            // 
            this.Start.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(20)))));
            this.Start.Location = new System.Drawing.Point(581, 324);
            this.Start.Name = "Start";
            this.Start.Size = new System.Drawing.Size(94, 35);
            this.Start.Style = MetroFramework.MetroColorStyle.White;
            this.Start.TabIndex = 3;
            this.Start.TabStop = false;
            this.Start.Text = "Start";
            this.Start.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.Start.UseCustomBackColor = true;
            this.Start.UseSelectable = true;
            this.Start.Click += new System.EventHandler(this.Start_Click);
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel1.Location = new System.Drawing.Point(123, 298);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(0, 0);
            this.metroLabel1.TabIndex = 4;
            this.metroLabel1.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroLabel1.UseCustomBackColor = true;
            // 
            // metroProgressBar2
            // 
            this.metroProgressBar2.Location = new System.Drawing.Point(105, 324);
            this.metroProgressBar2.Name = "metroProgressBar2";
            this.metroProgressBar2.Size = new System.Drawing.Size(435, 35);
            this.metroProgressBar2.Step = 1;
            this.metroProgressBar2.Style = MetroFramework.MetroColorStyle.Lime;
            this.metroProgressBar2.TabIndex = 0;
            this.metroProgressBar2.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroProgressBar2.UseCustomBackColor = true;
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel2.Location = new System.Drawing.Point(123, 362);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(0, 0);
            this.metroLabel2.TabIndex = 5;
            this.metroLabel2.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroLabel2.UseCustomBackColor = true;
            // 
            // CancelButton
            // 
            this.CancelButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(20)))));
            this.CancelButton.Enabled = false;
            this.CancelButton.Location = new System.Drawing.Point(581, 260);
            this.CancelButton.Name = "CancelButton";
            this.CancelButton.Size = new System.Drawing.Size(94, 35);
            this.CancelButton.Style = MetroFramework.MetroColorStyle.White;
            this.CancelButton.TabIndex = 2;
            this.CancelButton.TabStop = false;
            this.CancelButton.Text = "Cancel";
            this.CancelButton.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.CancelButton.UseCustomBackColor = true;
            this.CancelButton.UseSelectable = true;
            this.CancelButton.Visible = false;
            this.CancelButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // Pérenne
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BackImage = global::WindowsFormsApplication3.Properties.Resources._800x400;
            this.BackLocation = MetroFramework.Forms.BackLocation.BottomLeft;
            this.BackMaxSize = 800;
            this.ClientSize = new System.Drawing.Size(800, 397);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.metroProgressBar2);
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.Start);
            this.Controls.Add(this.CheckFile);
            this.Controls.Add(this.metroProgressBar1);
            this.Controls.Add(this.CancelButton);
            this.Font = new System.Drawing.Font("Arial", 24.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Pérenne";
            this.Resizable = false;
            this.Style = MetroFramework.MetroColorStyle.Black;
            this.Text = "PÉRENNE";
            this.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.Load += new System.EventHandler(this.Pérenne_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroProgressBar metroProgressBar1;
        private MetroFramework.Controls.MetroButton Start;
        private MetroFramework.Controls.MetroButton CheckFile;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroProgressBar metroProgressBar2;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroButton CancelButton;
    }
}

