﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Security.Cryptography;
using System.IO.Compression;
using System.Diagnostics;



namespace WindowsFormsApplication3
{
    public partial class Pérenne : MetroFramework.Forms.MetroForm
    {
        // Global variables
        //number of files listed in the hash.txt
        public int NUMBER_OF_FILES;
        //is the first read of hash.txt ?
        public bool IS_FIRST_READ = true;
        //is download canceled ?
        public bool CANCELED = false;


        public Pérenne()
        {
            InitializeComponent();
        }


        private void Pérenne_Load(object sender, EventArgs e)
        {

        }

        private void CheckFile_Click(object sender, EventArgs e)
        {
            CANCELED = false;
            CheckFile.Visible = false;            
            CheckFile.Enabled = false;
            CancelButton.Visible = true;
            CancelButton.Enabled = true;
            Start.Enabled = false;
            //we refresh to 0 the purcent of the 2nd progress bar
            metroProgressBar2.Value = 0;
            //we dl the hash.txt file who is a list of files and checksum
            WebClient webClient = new WebClient();
            //on completed we go to callback Completed
            webClient.DownloadFileCompleted += new AsyncCompletedEventHandler(Completed);
            webClient.DownloadProgressChanged += ProgressChanged(webClient);
            //url of the file and local position where to dl it
            webClient.DownloadFileAsync(new Uri("http://164.132.203.159/launcher/hash.txt"), @"hash.txt");
        }
        public DownloadProgressChangedEventHandler ProgressChanged(WebClient webClient)
        {
            Action<object, DownloadProgressChangedEventArgs> action = (sender, e) =>
            {

                if (!CANCELED)
                {// we update the progress bar for each file
                    metroProgressBar1.Value = e.ProgressPercentage;
                }
                else
                {
                    webClient.CancelAsync();
                }


            };
            return new DownloadProgressChangedEventHandler(action);

        }

        private void Completed(object sender, AsyncCompletedEventArgs e)
        {
            //we refresh to 0 the purcent of the 1st progress bar
            metroProgressBar1.Value = 0;

            if ( IS_FIRST_READ)
            {
                //we open the file and read all line and put them into a variable
                var howMuchLines = File.ReadAllLines("hash.txt");
                //we get the lenght of the variable and btw the number of files to check/download
                NUMBER_OF_FILES = howMuchLines.Length;
                //it was the first time we read the file, now we put the value to false
                IS_FIRST_READ = !IS_FIRST_READ;
            }
            //we open the file and read the last line
            string lastLine = File.ReadLines("hash.txt").Last();

            //we separate the line in an array of string
            String[] substrings = lastLine.Split(':');
            //we get path to the file here
            string path_to_file = substrings[0];
            //we get hashcode here
            string hashcode = substrings[1];


            //we separate again the 1st string with '\' (\u005c is '\')
            String[] folder_file_names = path_to_file.Split('\u005c');
            //we get filename here
            string fullFilename = folder_file_names[folder_file_names.Length - 1];

            //we get clean array of all folder to check/create localy
            List<string> localPathList = new List<string>();
            //string for web path
            string webPathString = "";
            //j = 1 for avoid the first occurence (j = 0 is a dot ".")
            localPathList.Add(folder_file_names[1]);
            webPathString += folder_file_names[1] + "/";
            //start loop at j=2 now
            for (int j = 2; j < (folder_file_names.Length - 1); j++)
            {
                localPathList.Add(folder_file_names[j - 1] + "\\" + folder_file_names[j]);
                webPathString += folder_file_names[j] + "/";
            }
            // we reconvert the list into array (maybe not usefull)
            string[] localPath = localPathList.ToArray();

            // we create all directory if they don't allready exist        
            for (int h = 0; h < (localPath.Length); h++)
            {
                if (!Directory.Exists(localPath[h]))
                {
                    Directory.CreateDirectory(localPath[h]);
                }
            }

            string localFileUrl = localPath[localPath.Length - 1] + "\\" + fullFilename;
            string hashcodeLocal = checkMD5(localFileUrl);
            //we have to remove some invisible char(\n, \o, ...) in hashcodeLocal and hashcode
            var charsToRemove = new char[] { '\u000D', '\u000A', '\u200c', '\u200b' };
            foreach (var c in charsToRemove)
            {
                hashcodeLocal = hashcodeLocal.Replace(c.ToString(), string.Empty);
                hashcode = hashcode.Replace(c.ToString(), string.Empty);
            }

            //if hashcode aren't equals or file not exist we download the file
            if (!hashcodeLocal.Equals(hashcode) || hashcodeLocal == "nofile")
            {
                WebClient webClient = new WebClient();
                //we add .zip because files are ziped for dl we'll unzip on callback
                webClient.DownloadFileCompleted += DownloadFileCompleted(localFileUrl , localPath[localPath.Length - 1]);
                //webClient.DownloadProgressChanged += new DownloadProgressChangedEventHandler(ProgressChanged);
                webClient.DownloadProgressChanged += ProgressChanged(webClient);
                string vartodl = localFileUrl + ".zip";
                webClient.DownloadFileAsync(new Uri("http://164.132.203.159/launcher/"+ webPathString+ fullFilename + ".zip"), vartodl);
                //we update the label to make it fit the actual file download
                metroLabel1.Text = localFileUrl;
            }
            // if file exist we bypass
            else
            {
                //we get all lines of hash
                var lines = File.ReadAllLines("hash.txt");
                // if it's not the last file to dl
                if (lines.Length > 1)
                {
                    //we delete the last line of the file
                    File.WriteAllLines("hash.txt", lines.Take(lines.Length - 1).ToArray());
                    //we calculate the progress purcentage
                    decimal percentDone = (((decimal)NUMBER_OF_FILES - (decimal)lines.Length) / (decimal)NUMBER_OF_FILES)*(decimal)100;
                    //we round the progress purcentage 2 decimal after 0
                    percentDone = Math.Round(percentDone, 2);
                    //we update to UI the progress
                    metroLabel2.Text = percentDone.ToString() + "%";
                    metroProgressBar2.Value = (int)percentDone;
                    //we loop as the file wasn't the last
                    Completed(sender, e);
                    
                }
                //if it's the last file
                else
                {
                    //we delete hash.txt
                    File.Delete("hash.txt");
                    //we update to UI the progress
                    metroLabel2.Text = "Download complete";
                    metroProgressBar1.Value = 100;
                    metroProgressBar2.Value = 100;                    
                    CheckFile.Visible = true;
                    CheckFile.Enabled = true;
                    CancelButton.Visible = false;
                    CancelButton.Enabled = false;
                    Start.Enabled = true;
                    //END
                }
            }
            




        }
        public AsyncCompletedEventHandler DownloadFileCompleted(string filename, string pathToUnzip)
        {
            Action<object, AsyncCompletedEventArgs> action = (sender, e) =>
            {
                //throw error (for 404 not found mainly)
                
                if (e.Cancelled)
                {
                    //we delete it and the hash.txt
                    if (File.Exists(filename + ".zip"))
                    {
                        File.Delete(filename + ".zip");
                    }
                    File.Delete("hash.txt");
                    metroLabel2.Text = "Download cancelled - Starting game is unsafe";
                    CheckFile.Visible = true;
                    CheckFile.Enabled = true;
                    CancelButton.Visible = false;
                    CancelButton.Enabled = false;
                    Start.Enabled = true;
                    IS_FIRST_READ = true;
                    //END
                    return;
                }
                if (e.Error != null)
                {
                    throw e.Error;
                    return;
                }
                //we unzip the zip file to directory
                //ZipFile.ExtractToDirectory(filename + ".zip", pathToUnzip);

                //Opens the zip file up to be read
                using (ZipArchive archive = ZipFile.OpenRead(filename + ".zip"))
                {
                    //Loops through each file in the zip file
                    foreach (ZipArchiveEntry file in archive.Entries)
                    {
                        //Extracts the files to the output folder in a safer manner
                        if (!File.Exists(filename))
                        {
                            //Creates the directory (if it doesn't exist) for the new path
                            Directory.CreateDirectory(pathToUnzip);
                            //Extracts the file to (potentially new) path
                            file.ExtractToFile(filename);
                        }
                    }
                }
                //we delete the zip file
                File.Delete(filename + ".zip");

                //we get all lines of hash
                if (File.Exists("hash.txt"))
                {
                    var lines = File.ReadAllLines("hash.txt");
                    // if it's not the last file to dl
                    if (lines.Length > 1)
                    {
                        //we delete the last line of the file
                        File.WriteAllLines("hash.txt", lines.Take(lines.Length - 1).ToArray());
                        //we calculate the progress purcentage
                        decimal percentDone = (((decimal)NUMBER_OF_FILES - (decimal)lines.Length) / (decimal)NUMBER_OF_FILES) * (decimal)100;
                        //we round the progress purcentage 2 decimal after 0
                        percentDone = Math.Round(percentDone, 2);
                        //we update to UI the progress
                        metroLabel2.Text = percentDone.ToString() + "%";
                        metroProgressBar2.Value = (int)percentDone;
                        //we loop as the file wasn't the last
                        Completed(sender, e);

                    }
                    //if it's the last file
                    else
                    {
                        //we delete hash.txt
                        File.Delete("hash.txt");
                        //we update to UI the progress
                        metroLabel2.Text = "Download complete";
                        metroProgressBar1.Value = 100;
                        metroProgressBar2.Value = 100;
                        CheckFile.Visible = true;
                        CheckFile.Enabled = true;
                        CancelButton.Visible = false;
                        CancelButton.Enabled = false;
                        Start.Enabled = true;
                        return;
                        //END
                    }
                }

            };
            return new AsyncCompletedEventHandler(action);
        }

        public string checkMD5(string filename)
        {
            //if file exist
            if (File.Exists(filename))
            {
                //we get the MD5 hashcode
                using (var md5 = MD5.Create())
                {
                    using (var stream = File.OpenRead(filename))
                    {
                        //we convert MD5 to string (result need some cleaning work, need to enhance this)
                        return BitConverter.ToString(md5.ComputeHash(stream)).Replace("-", "‌​").ToLower();
                    }
                }
            }
            //if file not exist
            else
            {
                return "nofile";
            }
        }

        private void Start_Click(object sender, EventArgs e)
        {

            if (File.Exists("system\\l2.exe"))
            {
                Process.Start(@"system\l2.exe");
                Close();
            }               
            
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            CANCELED = true;
            File.Delete("hash.txt");
        }
    }
}
